from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
# these two steps were added to  makesure the ingredients
#  and steps are written in an ordered list and not just one long sentence.
    steps = recipe.steps.splitlines()
    ingredients = recipe.ingredients.splitlines()
    context = { "recipe_object": recipe,
    "steps" : steps,
    "ingredients": ingredients

    }
    return render(request, "recipes/detail.html", context)

# Create your views here.
def recipe_list(request):
    recipe= Recipe.objects.all()
    context = {
        "recipe_list": recipe,
    }
    return render(request, "recipes/list.html", context)

def create_recipe(request):
    if request.method == "POST":
        form= RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = { "form": form,}
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    edited_recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form= RecipeForm(request.POST, instance= edited_recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=edited_recipe)
    context = { "recipe_object": edited_recipe,
        "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)